/**
 * 
 */
package com.example.foodieapp.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class UserOrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_order_id")
	private Long userOrderId;

	@Column(name = "vendor_id")
	private Long vendorId;

	@Column(name = "amount")
	private Double amount;

	private Long orderNumber;

	private LocalDateTime orderDate;

	private String orderStatus;

	@OneToMany(mappedBy = "userOrder")
	private List<OrderItem> orderItems;

	@ManyToOne
	@JoinColumn(name ="user_id")
	private User user;

}
